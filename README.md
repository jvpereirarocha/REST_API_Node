# RestAPI-node
A REST API built with Node JS

## Requisitos:
- Node v12.16.1

### Instruções:
1. Instalar o NodeJS na versão especificada acima.

2. Clonar o projeto para seu diretório de preferência. A estrutura de pastas é a seguinte:

  ![Example](https://raw.githubusercontent.com/jvpereirarocha/restAPI-node/master/estrutura_pastas.png)
  
  Obs: **A pasta node_modules não estará no repositório e será gerada automaticamente ao rodar o projeto**

3. Após estar na raíz do projeto, para instalar as dependencias, é necessário dar o comando:
  ` npm install `. Todos as dependencias do projeto serão baixadas

4. Após dar o comando `npm init`, e todos os arquivos estiverem baixados, execute o comando `npm start` ou `node server.js`
para rodar a aplicação.

5. Após esse procedimento, a API estará rodando. Para rodar os testes unitários execute o comando `npm test` e os resultados
dos testes irão aparecer no console
