function handlingRequestBodyNumbers(numObject) {
    values = []

    for (n in numObject) {
        values.push(parseInt(numObject[n]))
    }

    return values
}


function createObject(numObject) {
    return Object(numObject)
}

module.exports = { handlingRequestBodyNumbers, createObject }
