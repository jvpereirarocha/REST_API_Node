//Definições e imports
const port = 3005
const express = require('express')
const app = express()

const bodyParser = require('body-parser')

const numbers = require('./src/numbers')
const Number = numbers.Numbers

const handling = require('./handler')
// Fim das definições e imports


// Requisições
app.use(bodyParser.urlencoded({ extended: true }))

let obj = {}
let values = []


app.post('/adicionar', (request, response) => {

    obj = handling.createObject(request.body)
    values = handling.handlingRequestBodyNumbers(Object.values(obj))
    if (values.length > 0) {
        response.json({
            'Info': `Os números [${values}] foram adicionados com sucesso!`,
            'Obj': obj,
        })
    }
    else {
        response.json({ 'Info': 'Nenhum valor adicionado!' })
    }
})


app.get('/soma', (request, response) => {

    const sumTotal = getSomaTotal(request)
    if (sumTotal == 0) {
        return response.json({ 'Info': 'Sem valores a serem somados' })
    }
    return response.json({ 'Soma': sumTotal })
})


app.get('/media', (request, response) => {

    const avgNumbers = getAverage(request)
    if (avgNumbers == 0) {
        return response.json({ 'Info': 'Sem valores para fazer média' })
    }

    return response.json( { media: avgNumbers } )
})

app.delete('/excluir', (request, response) => {
    const deleteKeys = deleteKeysFromObject(request.query)
    return response.json({
        "Novo Objeto": deleteKeys,
    })
})


app.listen(port, () => {
    console.log(`The server is running at the port ${port}.`)
})
// Fim das requisições


//Funções auxiliares
const getInstanceNumberClass = (request) => {
    return new Number(values)
}


const getSomaTotal = (request) => {
    const numInstance = getInstanceNumberClass(request)
    return numInstance.sumNumbers()
}


const getAverage = (request) => {
    const numInstance = getInstanceNumberClass(request)
    return numInstance.averageNumbers()
}


const deleteKeysFromObject = (numObject) => {

    const keysParams = Object.keys(numObject)

    for (key in obj) {
        for(let i = 0; i < keysParams.length; i++) {
            if (key == keysParams[i]) {
                delete obj[key]
            }
        }
    }

    return obj
}

//Fim das funções auxiliares
