class Numbers {
    constructor(numbers) {
        this.numbers = numbers
    }

    getSumItems(array) {
        let sum = 0
        for (let j = 0; j < array.length; j++) {
            sum += array[j]
        }
        return sum
    }

    sumNumbers() {
        let sum = 0
        sum = this.getSumItems(this.numbers)
        return sum
    }

    averageNumbers() {
         let total = this.getSumItems(this.numbers)
         let media = 0

         if (this.numbers.length != 0) {
             media = parseFloat((total/this.numbers.length).toFixed(1))
         }

         return media
    }
}

module.exports = { Numbers }
