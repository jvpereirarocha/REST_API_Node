const test = require('tape')
const numbers = require('./src/numbers')
const Number = numbers.Numbers

const array = [2, 4, 6, 8]

const nums = new Number(array)


test('Somar vetor', (t) => {
    t.assert(nums.sumNumbers() === 20, "Soma calculada corretamente!")
    t.end()
})

test('Média aritmética', (t) => {
    t.assert(nums.averageNumbers() === 5.0, "Média aritmética calculada corretamente!")
    t.end()
})
